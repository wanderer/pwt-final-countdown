using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.AspNetCore.Hosting;
using pwt_final_countdown.Models;
using pwt_final_countdown.Models.Dbfake;

namespace pwt_final_countdown.Areas.PCKomponenty.Controllers
{
    [Area("PCKomponenty")]
    public class ProcessorController : Controller
    {
        IWebHostEnvironment hosting_env;
        private IList<Processor> Processors = Dbfake.Processors;

        public ProcessorController(IWebHostEnvironment hosting_env){
            this.hosting_env = hosting_env;
        }

        public IActionResult All()
        {
            ProcessorViewModel processor = new ProcessorViewModel();
            processor.Processors = Processors;
            return View(processor);
        }

        // GET
        public IActionResult Select()
        {
            ProcessorViewModel processor = new ProcessorViewModel();
            processor.Processors = Processors;
            return View(processor);
        }

        public IActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public async Task<IActionResult> Create(Processor processor)
        {
            processor.ImageSrc = string.Empty;
            MegaUpload mega_upload = new MegaUpload(hosting_env);
            await mega_upload.DoMegaUpload(processor);

            Processors.Add(processor);
            return RedirectToAction(nameof(Select));
        }

        public IActionResult Edit(int id)
        {
            Processor processor_item = Processors.Where(p_item => p_item.id == id).FirstOrDefault();
            if (processor_item != null)
            {
                return View(processor_item);
            }
            else
            {
                return NotFound();
            }
        }

        [HttpPost]
        public async Task<IActionResult> Edit(Processor processor)
        {
            Processor processor_item = Processors.Where(p_item => p_item.id == processor.id).FirstOrDefault();

            if (processor_item != null)
            {
                processor_item.id = processor.id;
                processor_item.DataTarget = processor.DataTarget;
                processor_item.ImageAlt = processor.ImageAlt;
                processor_item.Description = processor.Description;

                MegaUpload mega_upload = new MegaUpload(hosting_env);
                if (await mega_upload.DoMegaUpload(processor)){
                    processor_item.ImageSrc = processor.ImageSrc;
                }
                return RedirectToAction(nameof(Select));
            }
            else
            {
                return NotFound();
            }
        }

        public IActionResult Delete(int id)
        {
            Processor processor_item = Processors.Where(p_item => p_item.id == id).FirstOrDefault();
            if (processor_item != null)
            {
                Processors.Remove(processor_item);
                return RedirectToAction(nameof(Select));
            }
            else
            {
                return NotFound();
            }
        }
    }
}
