FROM mcr.microsoft.com/dotnet/core/sdk:3.1-alpine
ENV UID 1000
ENV GID 1000
ENV UNAME unpriv
RUN adduser -D -u ${UID} -g ${GID} -H ${UNAME} -h /src
WORKDIR /src

COPY global.json ./
RUN sed -i 's/3.1.110/3.1.404/' global.json

COPY *.csproj ./
RUN dotnet restore

COPY . ./
RUN sed -i 's/3.1.110/3.1.404/' global.json
RUN dotnet publish -c Release -o bin/out

FROM mcr.microsoft.com/dotnet/aspnet:3.1
WORKDIR /App
COPY --from=0 /src/bin/out/ .
RUN chown -R ${UID}:${GID} ./
USER ${UNAME}
ENV ASPNETCORE_ENVIRONMENT=Release
ENTRYPOINT ["dotnet", "pwt-0x01-ng.dll"]
