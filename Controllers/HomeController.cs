﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using pwt_final_countdown.Models;
using pwt_final_countdown.Models.Dbfake;

namespace pwt_final_countdown.Controllers
{
    public class HomeController : Controller
    {
        private IList<Processor> Processors = Dbfake.Processors;

        public IActionResult Index()
        {
            ProcessorViewModel processor = new ProcessorViewModel();
            processor.Processors = Processors;
            return View(processor);
        }

        public IActionResult About()
        {
            ViewData["Message"] = "About this awesome app.";

            return View();
        }

        public IActionResult Contact()
        {
            ViewData["Message"] = "Your contact page.";

            return View();
        }

        public IActionResult Privacy()
        {
            return View();
        }

        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }
    }
}
