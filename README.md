# pwt-final-countdown

this repo holds *sawce* for PWT .netcore mvc project final-countdown

### how to run this
> run the following commands from the solution folder

on the first run, restore stuff
```sh
dotnet restore
```

build and run
```sh
dotnet build && dotnet run
```
#### errors
if you get a weird long error about not being able to listen bind a port, make sure nothing else is listening on the port *this* thing is trying to bind (tcp/5000).  
if something else *is* already listening, solve it by killing it before running `dotnet run` or change the app port in [[Properties/launchSettings.json]]
