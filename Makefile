dtag = netcorezapocet
dtagdev = netcorezapocet-dev
dfile = Dockerfile
dfiledev = $(dfile).dev
lport = 8000
lportdev = 8001
CC = dotnet
dcmd = docker
pruneargs = system prune -af
dcmdrun = $(dcmd) run --rm
wdir = /src
kanikoimg = gcr.io/kaniko-project/executor
dargskaniko = -w=$(wdir) -v $$(pwd):$(wdir) $(kanikoimg)
kanikoargs = -c=$(wdir) --use-new-run --snapshotMode=redo --no-push
krelease = $(dcmdrun) $(dargskaniko) -f=$(dfile) $(kanikoargs)
kdebug = $(dcmdrun) $(dargskaniko) -f=$(dfiledev) $(kanikoargs)

.PHONY: dev dockerbuild dockerdevbuild dockerrun dockerdevrun dockertest dockerdev kaniko clean prune test

dev: restore build run

restore:
	$(CC) restore

build:
	$(CC) build .

run:
	$(CC) watch run . --no-restore

releasebuild: restore clean
	$(CC) publish -c Release

dockerbuild:
	docker build \
		--build-arg VCS_REF=`git rev-parse --short HEAD` \
		--build-arg BUILD_DATE=`date -u +"%Y-%m-%dT%H:%M:%SZ"` \
		-t $(dtag) -f $(dfile) .

dockerdevbuild:
	docker build \
		--build-arg VCS_REF=`git rev-parse --short HEAD` \
		--build-arg BUILD_DATE=`date -u +"%Y-%m-%dT%H:%M:%SZ"` \
		-t $(dtagdev) -f $(dfiledev) .

dockerrun:
	@echo ======================
	@echo local port: $(lport)
	@echo ======================
	$(dcmdrun) -p $(lport):80 $(dtag)

dockerdevrun:
	@echo ======================
	@echo local dev port: $(lportdev)
	@echo ======================
	$(dcmdrun) -p $(lportdev):5000 $(dtagdev)

dcdevrun:
	@echo ======================
	@echo local dev port: $(lportdev)
	@echo ======================
	docker-compose up --build --remove-orphans

kaniko:
	$(krelease)
	$(kdebug)

dockerdev: dockerdevbuild dockerdevrun

dockertest: dockerdevbuild dockerbuild

test: releasebuild build dockertest kaniko

clean:
	$(CC) clean

prune:
	$(dcmd) $(pruneargs)
