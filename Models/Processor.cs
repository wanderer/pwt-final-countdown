using Microsoft.AspNetCore.Http;

namespace pwt_final_countdown.Models
{
    public class Processor
    {
        public int id { get; set; }
        public string name { get; set; }
        public int clock_speed { get; set; }
        public string socket { get; set; }
        public int cores { get; set; }
        public int generation { get; set; }
        public int price { get; set; }
        public string DataTarget { get; set; }
        public IFormFile Image { get; set; }
        public string ImageSrc { get; set; }
        public string ImageAlt { get; set; }
        public string Description { get; set; }
    }
}
