using System.Collections.Generic;

namespace pwt_final_countdown.Models.Dbfake
{
    public static class ProcessorHelper
    {
        public static IList<Processor> GenerateProcessors()
        {
            IList<Processor> processors = new List<Processor>()
            {
                new Processor() { id = 0, name = "whatever", clock_speed=5, socket="SOMELGA", cores=256, generation=1, price=9001, DataTarget = "#myCarousel", ImageSrc = "/images/banner1.svg", ImageAlt = "ASP.NET", Description = "Learn how to build ASP.NET apps that can run anywhere.<a class=\"btn btn-default\" href=\"https://go.microsoft.com/fwlink/?LinkID=525028&clcid=0x409\">Learn More</a>"},
                new Processor() { id = 1, name = "whatever", clock_speed=5, socket="SOMELGA", cores=256, generation=1, price=9001, DataTarget = "#myCarousel", ImageSrc = "/images/banner2.svg", ImageAlt = "ASP.NET", Description = "There are powerful new features in Visual Studio for building modern web apps.<a class=\"btn btn-default\" href=\"https://go.microsoft.com/fwlink/?LinkID=525030&clcid=0x409\">Learn More</a>"},
                new Processor() { id = 2, name = "whatever", clock_speed=5, socket="SOMELGA", cores=256, generation=1, price=9001, DataTarget = "#myCarousel", ImageSrc = "/images/banner3.svg", ImageAlt = "ASP.NET", Description = "Learn how Microsoft's Azure cloud platform allows you to build, deploy, and scale web apps.<a class=\"btn btn-default\" href=\"https://go.microsoft.com/fwlink/?LinkID=525027&clcid=0x409\">Learn More</a>"},
                new Processor() { id = 3, name = "whatever", clock_speed=5, socket="SOMELGA", cores=256, generation=1, price=9001, DataTarget = "#myCarousel", ImageSrc = "/images/ms_loves_linux.jpeg", ImageAlt = "msloveslinux", Description = "ms loves linux"}
            };
            return processors;
        }
    }
}
