using System.Collections.Generic;

namespace pwt_final_countdown.Models.Dbfake
{
    public static class Dbfake
    {
        /* singe db table simulation*/
        public static IList<Processor> Processors { get; set; }

        static Dbfake()
        {
            Processors = ProcessorHelper.GenerateProcessors();
        }
    }
}
