using System.Collections.Generic;

namespace pwt_final_countdown.Models
{
    public class ProcessorViewModel
    {
        public IList<Processor> Processors { get; set; }
    }
}
