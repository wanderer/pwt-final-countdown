using System;
using System.Collections.Generic;
using System.IO;
using Microsoft.AspNetCore.Hosting;
using System.Linq;
using System.Threading.Tasks;

namespace pwt_final_countdown.Models
{
    public class MegaUpload
    {
        IWebHostEnvironment hosting_env;

        public MegaUpload(IWebHostEnvironment hosting_env){
            this.hosting_env = hosting_env;
        }
        public async Task<bool> DoMegaUpload(Processor processor)
        {
            bool great_success = false;
            var img = processor.Image;
            if(img != null && img.ContentType.ToLower().Contains("image") && img.Length > 0 && img.Length < 2000000){
                var fname = Path.GetFileNameWithoutExtension(img.FileName);
                var fext = Path.GetExtension(img.FileName);
                /* in case such file already exists - wip */
                /* var fname_rand = Path.GetRandomFileName()+Path.GetFileNameWithoutExtension(img.FileName); */
                var f_relative = Path.Combine("images","processors", fname + fext);
                var file_path = Path.Combine(hosting_env.WebRootPath, f_relative);

                using (var stream = new FileStream(file_path, FileMode.Create)){
                    await img.CopyToAsync(stream);
                }
                processor.ImageSrc = $"/{f_relative}";
                great_success = true;
            }
            return great_success;
        }
    }
}